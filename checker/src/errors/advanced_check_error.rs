use super::{print_error, print_file, print_path, Highlight};
use std::path::PathBuf;
use termcolor::{Color, ColorChoice, StandardStream};

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct AdvancedCheckError {
    pub path: PathBuf,
    pub kind: Kind,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Kind {
    AbsentImage { line: usize },
    CorruptedImage,
}

pub fn display(error: &AdvancedCheckError) {
    let stderr = &mut StandardStream::stderr(ColorChoice::Always);

    match error.kind {
        Kind::AbsentImage { line } => {
            print_error(stderr, "Could not find the image");
            print_path(stderr, &error.path);

            let file = std::fs::read_to_string(&error.path).unwrap();

            print_file(
                stderr,
                file.lines().skip(line),
                line,
                Some(Highlight {
                    range: line..=line,
                    color: Color::Red,
                }),
            );
        }
        Kind::CorruptedImage => {
            print_error(stderr, "Corrupted image");
            print_path(stderr, &error.path);
        }
    }
}
