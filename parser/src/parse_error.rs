use std::path::PathBuf;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ParseError {
    pub path: PathBuf,
    // Must be 0-based
    pub line: usize,
    pub kind: Kind,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Kind {
    NoInfoToml,
    InfoTomlParseError {
        inner: toml::de::Error,
    },
    EmptyFile,
    FileStartsWithNotHeading,
    InvalidVariableOpeningTag,
    InvalidVariableColor,
    NoVariableName {
        expected_level: usize,
    },
    WrongHeadingLevel {
        current_level: usize,
        expected_level: usize,
    },
    NoFigureImage,
    NoFigcaption,
    NoCaption,
    InvalidCaption,
    InvalidFigureEnd,
    UnknownHtml,
    InlineImage,
}
