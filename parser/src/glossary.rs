use std::path::PathBuf;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Meta {
    pub line: usize,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Glossary {
    pub language_name: String,
    pub title: String,
    pub search: String,
    pub about: String,
    pub sections: Vec<TopLevelSection>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Section {
    pub meta: Meta,
    pub title: String,
    pub contents: Vec<Contents>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct TopLevelSection {
    pub path: PathBuf,
    pub section: Section,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Paragraph {
    pub meta: Meta,
    pub items: Vec<ParagraphItem>,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Contents {
    Subsection(Section),
    Paragraph(Paragraph),
    Variable {
        meta: Meta,
        name: String,
        color: Color,
        description: Vec<Contents>,
    },
    Image {
        meta: Meta,
        url: String,
        alt: String,
        description: Paragraph,
    },
    List {
        start: Option<usize>,
        items: Vec<Vec<Contents>>,
    },
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum ParagraphItem {
    LineBreak,
    Text(String),
    Bold(Vec<ParagraphItem>),
    Italic(Vec<ParagraphItem>),
    Monospace(String),
    Strikethrough(Vec<ParagraphItem>),
    Link {
        text: Vec<ParagraphItem>,
        url: String,
    },
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Color {
    Red,
    Pink,
    Purple,
    DeepPurple,
    Indigo,
    Blue,
    LightBlue,
    Cyan,
    Teal,
    Green,
    LightGreen,
    Lime,
    Yellow,
    Amber,
    Orange,
    DeepOrange,
    Brown,
    Gray,
    BlueGray,
}
