# The .attheme glossary database parser

The repository provides a parser of the database, which can be used in tools
written in Rust.

## Installing

We do not intend to publish the parser on crates.io. Instead, pull the parser
from GitLab:

```toml
[dependencies.attheme_glossary_parser]
git = "https://gitlab.com/attheme-glossary/database.git"
```

## Usage

```rust
use std::path::Path;
use attheme_glossary_parser::parse;

let glossary = parse(Path::new("database/en"))?;
```
