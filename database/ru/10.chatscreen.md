# Экран чата

## Сообщения

<glossary-variable color="red">

### chat_selectedBackground

Задает цвет заднего фона при выделение сообщения.

</glossary-variable>

<figure>

![](./images/chatscreen.0.png)

<figcaption>

Красное — `chat_selectedBackground`.

</figcaption>
</figure>

## Панель сообщения

<glossary-variable color="red">

### chat_messagePanelBackground

Задает цвет фона панели сообщения.

</glossary-variable>

<glossary-variable color="green">

### chat_messagePanelHint

Задает цвет текста поля ввода на панели сообщения.

</glossary-variable>

<glossary-variable color="indigo">

### chat_messagePanelText

Задает цвет текста внутри панели сообщения.

</glossary-variable>

<glossary-variable color="orange">

### chat_messagePanelSend

Задает цвет иконки "Отравить" на панели сообщения.

</glossary-variable>

<glossary-variable color="blue">

### chat_messagePanelIcons

Задает цвет иконок внутри панели сообщения.

</glossary-variable>

<glossary-variable color="brown">

### chat_fieldOverlayText

Задает цвет текста на панели сообщения(например, присоединиться к каналу или
группе, отменить голосовое или видео сообщение, приглушить чат).

</glossary-variable>

<figure>

![](./images/chatscreen.1.png)

<figcaption>

Красное — `chat_messagePanelBackground`, зеленое — `chat_messagePanelHint`,
темно-синее — `chat_messagePanelText`, оранжевое — `chat_messagePanelSend`,
синее — `chat_messagePanelIcons`, коричневое — `chat_fieldOverlayText`.

</figcaption>
</figure>

## Панель эмодзи и стикеров

Если вы изменили эти переменные через редактор тем приложения, нужно перезайти
на экран чата, чтобы увидеть изменения.

<glossary-variable color="red">

### chat_emojiPanelBackground

Задает цвет фона панели для эмодзи, сохраненных стикеров, наборов стикеров в
превью и клавиатуры бота под панелью сообщения с прозрачностью с черным
значением.

</glossary-variable>

<glossary-variable color="green">

### chat_emojiPanelEmptyText

Задает цвет текста надписи "Нет часто используемых", если вы не использовали
эмодзи или очистили список, на вкладке с иконкой часов.

</glossary-variable>

<glossary-variable color="indigo">

### chat_emojiPanelIcon

Задает цвет всех иконок на горизонтальной прокручиваемой строке под панелью
сообщения, кроме выбранной вкладки.

</glossary-variable>

<glossary-variable color="purple">

### chat_emojiPanelIconSelected

Задает цвет иконки выбранной вкладки.

</glossary-variable>

<glossary-variable color="orange">

### chat_emojiPanelIconSelector

Задает цвет линии, показывающей текущее значение набора эмодзи на
chat_emojiPanelShadowLine.

</glossary-variable>

<glossary-variable color="blue">

### chat_emojiPanelBackspace

Задает цвет иконки удаления.

</glossary-variable>

<glossary-variable color="brown">

### chat_emojiPanelStickerPackSelector

Задает цвет фона квадрата на текущем выбранном наборе стикеров.

</glossary-variable>

<figure>

![](./images/chatscreen.2.png)

<figcaption>

Красное — `chat_emojiPanelBackground`, зеленое — `chat_emojiPanelEmptyText`,
темно-синее — `chat_emojiPanelIcon`, фиолетовое — `chat_emojiPanelIconSelected`,
оранжевое — `chat_emojiPanelIconSelector`, синее — `chat_emojiPanelBackspace`,
коричневое — `chat_emojiPanelStickerPackSelector`.

</figcaption>
</figure>

Следующие переменные можно изменить только с
[.attheme editor](http://snejugal.ru/attheme-editor).

<glossary-variable color="red">

### chat_emojiPanelStickerSetName

Задает цвет названия набора стикеров при прокрутке.

</glossary-variable>

<glossary-variable color="green">

### chat_emojiPanelStickerSetNameIcon

Задает цвет иконок удаления или настроек рядом со названием стикера.

</glossary-variable>

<figure>

![](./images/chatscreen.3.png)

<figcaption>

Красное — `chat_emojiPanelStickerSetName`, зеленое —
`chat_emojiPanelStickerSetNameIcon`.

</figcaption>
</figure>

<glossary-variable color="red">

### featuredStickers_delButton

Задает цвет фона кнопки "Удалить" в секции популярных стикеров.

</glossary-variable>

<glossary-variable color="indigo">

### featuredStickers_delButtonPressed

Задает цвет фона кнопки "Удалить" при нажатии.

</glossary-variable>

<glossary-variable color="green">

### featuredStickers_unread

Задает цвет синей точки около названия набора стикеров.

</glossary-variable>

<figure>

![](./images/chatscreen.4.png)

<figcaption>

Красное — `featuredStickers_delButton`, темно-синее —
`featuredStickers_delButtonPressed`, зеленое — `featuredStickers_unread`.

</figcaption>
</figure>

## Клавиатура бота

Фон панели — `chat_emojiPanelBackground`.

<glossary-variable color="red">

### chat_botKeyboardButtonText

Задает цвет текста на кнопках в панели под полосой сообщения.

</glossary-variable>

<glossary-variable color="indigo">

### chat_botKeyboardButtonBackground

Задает цвет фона кнопок.

</glossary-variable>

<glossary-variable color="green">

### chat_botKeyboardButtonBackgroundPressed

Задает цвет фона кнопки при нажатии, а также селектор этих кнопок. Это убирает
стандартный цвет, поэтому значение альфа-канала в 255 сделает кнопку прозрачной.

</glossary-variable>

<figure>

![](./images/chatscreen.5.png)

<figcaption>

Красное — `chat_botKeyboardButtonText`, темно-синее —
`chat_botKeyboardButtonBackground`, зеленое —
`chat_botKeyboardButtonBackgroundPressed`.

</figcaption>
</figure>
