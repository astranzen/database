# Фоны

<glossary-variable color="red">

## windowBackgroundWhite

Задаёт фон практически везде: например, фон списка чатов или настроек.

</glossary-variable>

<glossary-variable color="green">

## windowBackgroundGray

Задаёт второстепенный фон: например, фон в настройках между разделами, или фон
после истории звонков, если он не занимает весь экран.

</glossary-variable>

<figure>

![](./images/backgrounds.0.png)

<figcaption>

Красное ­— `windowBackgroundWhite`, зелёное — `windowBackgroundGray`.

</figcaption>
</figure>

<glossary-variable color="gray">

## chat_wallpaper

Задает цвет фона чата.

**Будьте осторожны:** если вы использовали картинку в качестве фона, телеграм
будет сжимать ее каждый раз, когда вы меняете любую переменную через редактор
тем приложения. Поэтому после всех внесенных изменений установите картинку
снова.

</glossary-variable>

<figure>

![](./images/backgrounds.1.png)

<figcaption>

Пример фона с картинкой.

</figcaption>
</figure>
