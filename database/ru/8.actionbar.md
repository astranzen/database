# Панель действий

## Обычный режим

<glossary-variable color="red">

### actionBarDefault

Задает цвет фона шапки приложения. Фон строки состояния генерируется
автоматически (на Android 5.0 и выше). Шапки экранов: плеера, настроек,
информации о чате, выделенных сообщений, имееют другие переменные.

</glossary-variable>

<glossary-variable color="lightBlue">

### actionBarDefaultIcon

Задает цвет иконок.

</glossary-variable>

<glossary-variable color="yellow">

### actionBarDefaultTitle

Задает цвет названия.

</glossary-variable>

<glossary-variable color="green">

### actionBarDefaultSelector

Задает цвет оверлея, который появляется при нажатии на иконку.

</glossary-variable>

<glossary-variable color="orange">

### actionBarDefaultSubtitle

Задает цвет текста под названием панели действий (к примеру, количество
участников, был(а) недавно, печатает...).

</glossary-variable>

<figure>

![](./images/actionbar.0.png)

<figcaption>

Красное — `actionBarDefault`, светло-синее — `actionBarDefaultIcon`, желтое —
`actionBarDefaultTitle`, зеленое — `actionBarDefaultSelector`, оранжевое —
`actionBarDefaultSubtitle`.

</figcaption>
</figure>

## Режим действий

Режим действий — это диалоговое окно верхней панели, которое открывается, для
ответа, пересылки или редактирования, выбора общих файлов, музыки, сообщений в
общедоступных медиа-сообщениях чата, а также при прикреплении файлов или
выделенном сообщении.

<glossary-variable color="red">

### actionBarActionModeDefault

Задает цвет фона перекрытия в режиме действий. Если установить прозрачность,
тогда будет видна стандартная панель действий.

</glossary-variable>

<glossary-variable color="blue">

### actionBarActionModeDefaultIcon

Задает цвет иконки на панели и номер количества выбранных элементов.

</glossary-variable>

<glossary-variable color="orange">

### actionBarActionModeDefaultSelector

Задает цвет оверлея, который появляется при нажаматии на иконку.

</glossary-variable>

<glossary-variable color="green">

### actionBarActionModeDefaultTop

Задает цвет панели состояния, но цвет все равно темнее на 20%. Установка
прозрачности в 0, также установит цвет панели состояния в значении
`actionBarActionModeDefault`.

</glossary-variable>

<figure>

![](./images/actionbar.1.png)

<figcaption>

Красное — `actionBarActionModeDefault`, зеленое —
`actionBarActionModeDefaultTop`, синее — `actionBarActionModeDefaultIcon`,
оранжевое — `actionBarActionModeDefaultSelector`.

</figcaption>
</figure>

## Подменю

Подменю появляется, когда вы нажимаете на иконку "..." на панели действий.

<glossary-variable color="red">

### actionBarDefaultSubmenuBackground

Задает цвет фона подменю.

</glossary-variable>

<glossary-variable color="blue">

### actionBarDefaultSubmenuItem

Задает цвет текста кнопок в подменю.

</glossary-variable>

<figure>

![](./images/actionbar.2.png)

<figcaption>

Красное — `actionBarDefaultSubmenuBackground`, синее —
`actionBarDefaultSubmenuItem`.

</figcaption>
</figure>

## Поиск

<glossary-variable color="red">

### actionBarDefaultSearchPlaceholder

Задает цвет текста, который вы видете, когда ничего не ввели в поле поиска.

</glossary-variable>

<glossary-variable color="green">

### actionBarDefaultSearch

Задает цвет вводимого текста в поле поиска.

</glossary-variable>

<figure>

![](./images/actionbar.3.png)

<figcaption>

Красное — `actionBarDefaultSearchPlaceholder`, зеленое —
`actionBarDefaultSearch`.

</figcaption>
</figure>

## Экран "Что такое канал?"

Этот экран появляется, при создании нового канала, если вы еще не разу его не
создавали. Вы можете изменить значение переменной только с
[.attheme editor](http://snejugal.ru/attheme-editor).

<glossary-variable color="red">

### actionBarWhiteSelector

Задает цвет оверлея при нажатии на кнопку.

</glossary-variable>

<figure>

![](./images/actionbar.4.png)

<figcaption>

Красное — `actionBarWhiteSelector`.

</figcaption>
</figure>
